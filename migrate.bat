docker run -v %cd%:/repo --network leflot --rm -e ASPNETCORE_ENVIRONMENT=Production -it mcr.microsoft.com/dotnet/sdk:5.0 sh -c "dotnet tool install --global dotnet-ef && export PATH="$PATH:/root/.dotnet/tools" && dotnet ef database update -p /repo/src/LeFlot -s /repo/src/LeFlot.Api"
docker-compose restart api
