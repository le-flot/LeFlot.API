#!/bin/sh

docker-compose down
docker volume rm leflotapi_pgdata
docker-compose up -d
sh migrate.sh
