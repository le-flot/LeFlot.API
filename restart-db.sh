#!/bin/sh

if type docker &> /dev/null; then
   DOCKER=docker
elif type podman &> /dev/null; then
   DOCKER=podman
else
   echo "Could not find docker or podman"
   exit 1
fi

echo "Docker client: ${DOCKER}"

container_name=le_flot_db
db_name=le_flot
db_port=5432

CONTAINER_IS_RUNNING_CHECK=$($DOCKER inspect $container_name 2> /dev/null)
if [ $? -eq 0 ]; then
    echo "Delete existing container..."
    $DOCKER rm -f $container_name
fi

echo "Run new container..."
$DOCKER run -d \
    --name $container_name \
    -p $db_port:5432 \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=postgres \
    -e POSTGRES_DB=$db_name \
    --restart=always \
    docker.io/library/postgres:13
