FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /build

# Copy csproj and restore as distinct layers
COPY *.sln .
COPY src/ src/
COPY tests/ tests/

RUN dotnet restore

# Build
RUN dotnet publish -c Release -o /app --no-restore

# Create final image
FROM mcr.microsoft.com/dotnet/aspnet:5.0
WORKDIR /app
COPY --from=build /app ./

EXPOSE 5000
ENTRYPOINT ["dotnet", "LeFlot.Api.dll"]
