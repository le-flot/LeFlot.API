using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LeFlot.Commands
{
    public sealed class RemoveOldConnectionRecordsCommand : IRequest
    {
        public RemoveOldConnectionRecordsCommand(DateTimeOffset removeBefore)
        {
            RemoveBefore = removeBefore;
        }

        public DateTimeOffset RemoveBefore { get; }

        internal sealed class Handler : IRequestHandler<RemoveOldConnectionRecordsCommand>
        {
            private readonly LeFlotDbContext _dbContext;

            public Handler(LeFlotDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<Unit> Handle(RemoveOldConnectionRecordsCommand request, CancellationToken cancellationToken)
            {
                var oldConnectionRecords = await _dbContext.ConnectionRecords
                    .Where(x => x.LastAccess < request.RemoveBefore)
                    .ToListAsync(cancellationToken);
                _dbContext.ConnectionRecords.RemoveRange(oldConnectionRecords);
                await _dbContext.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
