using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LeFlot.Commands
{
    public sealed class RemoveOldMessagesCommand : IRequest
    {
        public RemoveOldMessagesCommand(DateTimeOffset removeBefore)
        {
            RemoveBefore = removeBefore;
        }

        public DateTimeOffset RemoveBefore { get; }

        internal sealed class Handler : IRequestHandler<RemoveOldMessagesCommand>
        {
            private readonly LeFlotDbContext _dbContext;

            public Handler(LeFlotDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<Unit> Handle(RemoveOldMessagesCommand request, CancellationToken cancellationToken)
            {
                var oldMessages = await _dbContext.Messages
                    .Where(x => x.CreatedAt < request.RemoveBefore)
                    .ToListAsync(cancellationToken);
                _dbContext.Messages.RemoveRange(oldMessages);
                await _dbContext.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
