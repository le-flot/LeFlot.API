using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LeFlot.Commands
{
    public sealed class UpdateConnectionRecordLastAccessCommand : IRequest
    {
        public UpdateConnectionRecordLastAccessCommand(int id)
        {
            Id = id;
        }

        public int Id { get; }

        internal sealed class Handler : IRequestHandler<UpdateConnectionRecordLastAccessCommand>
        {
            private readonly LeFlotDbContext _dbContext;

            public Handler(LeFlotDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<Unit> Handle(
                UpdateConnectionRecordLastAccessCommand request,
                CancellationToken cancellationToken)
            {
                var connectionRecord = await _dbContext.ConnectionRecords
                    .Where(x => x.Id == request.Id)
                    .OrderBy(x => x.Id)
                    .FirstOrDefaultAsync(cancellationToken);

                if (connectionRecord != null)
                {
                    connectionRecord.LastAccess = DateTimeOffset.Now;
                    await _dbContext.SaveChangesAsync(cancellationToken);
                }

                return Unit.Value;
            }
        }
    }
}
