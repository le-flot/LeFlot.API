using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using MediatR;

namespace LeFlot.Commands
{
    public sealed class CreateConnectionRecordCommand : IRequest<ConnectionRecordEntity>
    {
        public CreateConnectionRecordCommand(IPAddress ipAddress)
        {
            IpAddress = ipAddress;
        }

        public IPAddress IpAddress { get; }

        internal sealed class Handler : IRequestHandler<CreateConnectionRecordCommand, ConnectionRecordEntity>
        {
            private readonly LeFlotDbContext _dbContext;

            public Handler(LeFlotDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<ConnectionRecordEntity> Handle(
                CreateConnectionRecordCommand request,
                CancellationToken cancellationToken)
            {
                var connectionRecord = new ConnectionRecordEntity
                {
                    IpAddress = request.IpAddress,
                    LastAccess = DateTimeOffset.Now,
                };

                var entry = _dbContext.ConnectionRecords.Add(connectionRecord);
                await _dbContext.SaveChangesAsync(cancellationToken);

                return entry.Entity;
            }
        }
    }
}
