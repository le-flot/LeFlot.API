using System;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using LeFlot.Models;
using MediatR;
using ProfanityFilter.Interfaces;

namespace LeFlot.Commands
{
    public sealed class CreateMessageCommand : IRequest<FilteredMessage>
    {
        public CreateMessageCommand(string text)
        {
            Text = text;
        }

        public string Text { get; }

        public int ObjectId { get; set; }

        internal sealed class Handler : IRequestHandler<CreateMessageCommand, FilteredMessage>
        {
            private readonly LeFlotDbContext _dbContext;
            private readonly IProfanityFilter _profanityFilter;

            public Handler(LeFlotDbContext dbContext, IProfanityFilter profanityFilter)
            {
                _dbContext = dbContext;
                _profanityFilter = profanityFilter;
            }

            public async Task<FilteredMessage> Handle(CreateMessageCommand request, CancellationToken cancellationToken)
            {
                var newMessage = new MessageEntity
                {
                    Text = request.Text,
                    ObjectId = request.ObjectId,
                    CreatedAt = DateTimeOffset.Now,
                };

                var entry = _dbContext.Messages.Add(newMessage);
                await _dbContext.SaveChangesAsync(cancellationToken);

                var filteredMessage = new FilteredMessage
                {
                    Id = entry.Entity.Id,
                    OriginText = entry.Entity.Text,
                    Text = _profanityFilter.CensorString(entry.Entity.Text),
                    ObjectId = entry.Entity.ObjectId,
                    CreatedAt = entry.Entity.CreatedAt,
                };

                return filteredMessage;
            }
        }
    }
}
