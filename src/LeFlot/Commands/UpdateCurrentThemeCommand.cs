using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using LeFlot.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LeFlot.Commands
{
    public sealed class UpdateCurrentThemeCommand : IRequest
    {
        public UpdateCurrentThemeCommand(int themeId)
        {
            ThemeId = themeId;
        }

        public int ThemeId { get; }

        internal sealed class Handler : IRequestHandler<UpdateCurrentThemeCommand>
        {
            private readonly LeFlotDbContext _dbContext;

            public Handler(LeFlotDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<Unit> Handle(UpdateCurrentThemeCommand request, CancellationToken cancellationToken)
            {
                var themeExists = await _dbContext.Themes
                    .Where(p => p.Id == request.ThemeId)
                    .AnyAsync(cancellationToken);

                if (!themeExists)
                {
                    throw new NotFoundException($"Could not find theme ID: {request.ThemeId}");
                }

                var appSettings = await _dbContext.AppSettings
                    .OrderBy(x => x.Id)
                    .FirstAsync(cancellationToken);

                appSettings.CurrentThemeId = request.ThemeId;

                await _dbContext.SaveChangesAsync(cancellationToken);

                return Unit.Value;
            }
        }
    }
}
