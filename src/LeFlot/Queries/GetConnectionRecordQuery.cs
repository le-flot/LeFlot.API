using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LeFlot.Queries
{
    public sealed class GetConnectionRecordQuery : IRequest<ConnectionRecordEntity>
    {
        public GetConnectionRecordQuery(IPAddress ipAddress)
        {
            IpAddress = ipAddress;
        }

        public IPAddress IpAddress { get; }

        internal sealed class Handler : IRequestHandler<GetConnectionRecordQuery, ConnectionRecordEntity>
        {
            private readonly LeFlotDbContext _dbContext;

            public Handler(LeFlotDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<ConnectionRecordEntity> Handle(
                GetConnectionRecordQuery request,
                CancellationToken cancellationToken)
            {
                return await _dbContext.ConnectionRecords
                    .Where(x => x.IpAddress == request.IpAddress)
                    .OrderBy(x => x.Id)
                    .FirstOrDefaultAsync(cancellationToken);
            }
        }
    }
}
