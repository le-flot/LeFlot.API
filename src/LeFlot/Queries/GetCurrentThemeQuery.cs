using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LeFlot.Queries
{
    public sealed class GetCurrentThemeQuery : IRequest<ThemeEntity>
    {
        internal sealed class Handler : IRequestHandler<GetCurrentThemeQuery, ThemeEntity>
        {
            private readonly LeFlotDbContext _dbContext;

            public Handler(LeFlotDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<ThemeEntity> Handle(GetCurrentThemeQuery request, CancellationToken cancellationToken)
            {
                var appSettings = await _dbContext.AppSettings
                    .OrderBy(x => x.Id)
                    .FirstAsync(cancellationToken);

                var theme = await _dbContext.Themes
                    .Where(p => p.Id == appSettings.CurrentThemeId)
                    .FirstAsync(cancellationToken);

                return theme;
            }
        }
    }
}
