using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using LeFlot.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProfanityFilter.Interfaces;

namespace LeFlot.Queries
{
    public sealed class GetMessagesQuery : IRequest<IEnumerable<FilteredMessage>>
    {
        internal sealed class Handler : IRequestHandler<GetMessagesQuery, IEnumerable<FilteredMessage>>
        {
            private readonly LeFlotDbContext _dbContext;
            private readonly IProfanityFilter _profanityFilter;

            public Handler(LeFlotDbContext dbContext, IProfanityFilter profanityFilter)
            {
                _dbContext = dbContext;
                _profanityFilter = profanityFilter;
            }

            public async Task<IEnumerable<FilteredMessage>> Handle(
                GetMessagesQuery request,
                CancellationToken cancellationToken)
            {
                var messages = await _dbContext.Messages
                    .OrderByDescending(x => x.CreatedAt)
                    .ToListAsync(cancellationToken);

                var filteredMessages = messages.Select(x =>
                    new FilteredMessage
                    {
                        Id = x.Id,
                        OriginText = x.Text,
                        Text = _profanityFilter.CensorString(x.Text),
                        ObjectId = x.ObjectId,
                        CreatedAt = x.CreatedAt,
                    });

                return filteredMessages;
            }
        }
    }
}
