using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using LeFlot.Exceptions;
using LeFlot.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProfanityFilter.Interfaces;

namespace LeFlot.Queries
{
    public sealed class GetMessageQuery : IRequest<FilteredMessage>
    {
        public GetMessageQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }

        internal sealed class Handler : IRequestHandler<GetMessageQuery, FilteredMessage>
        {
            private readonly LeFlotDbContext _dbContext;
            private readonly IProfanityFilter _profanityFilter;

            public Handler(LeFlotDbContext dbContext, IProfanityFilter profanityFilter)
            {
                _dbContext = dbContext;
                _profanityFilter = profanityFilter;
            }

            public async Task<FilteredMessage> Handle(GetMessageQuery request, CancellationToken cancellationToken)
            {
                var message = await _dbContext.Messages
                    .Where(p => p.Id == request.Id)
                    .FirstOrDefaultAsync(cancellationToken);

                if (message == null)
                {
                    throw new NotFoundException($"Could not find message ID: {request.Id}");
                }

                var filteredMessages = new FilteredMessage
                {
                    Id = message.Id,
                    OriginText = message.Text,
                    Text = _profanityFilter.CensorString(message.Text),
                    ObjectId = message.ObjectId,
                    CreatedAt = message.CreatedAt,
                };

                return filteredMessages;
            }
        }
    }
}
