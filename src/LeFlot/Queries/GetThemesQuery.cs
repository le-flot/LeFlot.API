using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LeFlot.Queries
{
    public sealed class GetThemesQuery : IRequest<IReadOnlyCollection<ThemeEntity>>
    {
        internal sealed class Handler : IRequestHandler<GetThemesQuery, IReadOnlyCollection<ThemeEntity>>
        {
            private readonly LeFlotDbContext _dbContext;

            public Handler(LeFlotDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<IReadOnlyCollection<ThemeEntity>> Handle(
                GetThemesQuery request,
                CancellationToken cancellationToken)
            {
                return await _dbContext.Themes
                    .OrderBy(x => x.Id)
                    .ToListAsync(cancellationToken);
            }
        }
    }
}
