using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using LeFlot.Data;
using LeFlot.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace LeFlot.Queries
{
    public sealed class GetThemeQuery : IRequest<ThemeEntity>
    {
        public GetThemeQuery(int id)
        {
            Id = id;
        }

        public int Id { get; }

        internal sealed class Handler : IRequestHandler<GetThemeQuery, ThemeEntity>
        {
            private readonly LeFlotDbContext _dbContext;

            public Handler(LeFlotDbContext dbContext)
            {
                _dbContext = dbContext;
            }

            public async Task<ThemeEntity> Handle(GetThemeQuery request, CancellationToken cancellationToken)
            {
                var theme = await _dbContext.Themes
                    .Where(p => p.Id == request.Id)
                    .FirstOrDefaultAsync(cancellationToken);

                if (theme == null)
                {
                    throw new NotFoundException($"Could not find theme ID: {request.Id}");
                }

                return theme;
            }
        }
    }
}
