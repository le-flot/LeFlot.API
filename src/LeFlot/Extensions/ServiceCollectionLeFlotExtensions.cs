using System.Reflection;
using LeFlot.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using ProfanityFilter.Interfaces;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Provides extension methods for registering LeFlot components to an <see cref="IServiceCollection"/>.
    /// </summary>
    public static class ServiceCollectionLeFlotExtensions
    {
        /// <summary>
        /// Adds LeFlot components to an <see cref="IServiceCollection"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to add LeFlot components to.</param>
        /// <param name="configuration">The configuration to use to get LeFlot options.</param>
        /// <returns>The <see cref="IServiceCollection"/> where all the Flo Projects' services were added to.</returns>
        public static IServiceCollection AddLeFlot(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<LeFlotDbContext>(options =>
                options.UseNpgsql(configuration.GetConnectionString("LeFlotContext")));

            services.AddMediatR(Assembly.GetExecutingAssembly());

            services.AddSingleton<IProfanityFilter, LeFlotProfanityFilter>();

            return services;
        }
    }
}
