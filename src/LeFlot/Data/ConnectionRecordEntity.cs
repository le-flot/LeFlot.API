using System;
using System.ComponentModel.DataAnnotations;
using System.Net;

namespace LeFlot.Data
{
    public class ConnectionRecordEntity
    {
        public int Id { get; set; }

        [Required]
        public IPAddress IpAddress { get; set; }

        public DateTimeOffset LastAccess { get; set; }
    }
}
