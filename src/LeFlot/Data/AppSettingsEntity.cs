using System.ComponentModel.DataAnnotations;

namespace LeFlot.Data
{
    public class AppSettingsEntity
    {
        public int Id { get; set; }

        [Required]
        public int CurrentThemeId { get; set; }
    }
}
