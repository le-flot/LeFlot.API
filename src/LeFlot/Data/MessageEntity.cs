using System;
using System.ComponentModel.DataAnnotations;

namespace LeFlot.Data
{
    public class MessageEntity
    {
        public int Id { get; set; }

        [Required]
        [MinLength(2)]
        [MaxLength(100)]
        public string Text { get; set; }

        public int ObjectId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }
    }
}
