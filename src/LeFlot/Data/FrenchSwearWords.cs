using System.Collections.Generic;

namespace LeFlot.Data
{
    /// <summary>
    /// This class contains the French profanity list.
    ///
    /// Inspired from https://fr.wiktionary.org/wiki/Cat%C3%A9gorie:Insultes_en_fran%C3%A7ais.
    /// </summary>
    /// <remarks>
    /// WARNING : This file contains a lot of very offensive terminology. Do not read the content of this source code
    /// file if you are easily offended.
    /// </remarks>
    internal static class FrenchSwearWords
    {
        public static readonly List<string> WordList = new()
        {
            "anus",
            "antisémite",
            "batard",
            "bâtard",
            "batards",
            "bâtards",
            "bite",
            "bites",
            "bouffon",
            "bouffons",
            "bougnoul",
            "bougnouls",
            "bougnoule",
            "bougnoules",
            "branleur",
            "branleurs",
            "caca",
            "calis",
            "caliss",
            "calisse",
            "câlisse",
            "chiennasse",
            "chiennasses",
            "chien sale",
            "chiens sales",
            "christ",
            "cibole",
            "colon",
            "colons",
            "con",
            "cons",
            "connard",
            "connards",
            "connasse",
            "connasses",
            "conne",
            "connes",
            "criss",
            "cris",
            "couille",
            "couilles",
            "crosseur",
            "crosseurs",
            "cul",
            "culs",
            "ducon",
            "dugland",
            "emmerdeur",
            "emmerdeurs",
            "emmerdeuse",
            "emmerdeuses",
            "enculé",
            "enculées",
            "enculer",
            "enculés",
            "enflure",
            "enflures",
            "enfoiré",
            "enfoirés",
            "esti",
            "fdp",
            "fils de pute",
            "fiotte",
            "fiottes",
            "fourrer",
            "garage à bite",
            "garage à bites",
            "garce",
            "kalisse",
            "kalisses",
            "merde",
            "mush",
            "nazi",
            "nazis",
            "negre",
            "nègre",
            "negres",
            "nègres",
            "negresse",
            "négresse",
            "negresses",
            "négresses",
            "negro",
            "négro",
            "negros",
            "négros",
            "nique",
            "nique sa mère",
            "nique sa mere",
            "nique ta mère",
            "nique ta mere",
            "osti",
            "pd",
            "pédale",
            "pedale",
            "pédales",
            "pedales",
            "pédé",
            "pédés",
            "pénis",
            "pet",
            "pets",
            "peteu",
            "pêteu",
            "peteux",
            "pêteux",
            "pipi",
            "piss",
            "pouffiasse",
            "pouffiasses",
            "poufiasse",
            "poufiasses",
            "putain",
            "pute",
            "putes",
            "sacrament",
            "salaud",
            "salauds",
            "salopard",
            "salopards",
            "salope",
            "salopes",
            "suce",
            "sti",
            "tabarn",
            "tabarnac",
            "tabarnack",
            "tabarnak",
            "tabarouette",
            "tafiole",
            "tafioles",
            "tafiolle",
            "tafiolles",
            "tantouze",
            "tantouzes",
            "tarlouze",
            "tarlouzes",
            "tata",
            "ta gueule",
            "ta yeule",
            "teubé",
            "teubés",
            "troudcul",
            "troudculs",
            "trouduc",
            "trouducs",
            "trou dcul",
            "trou de cul",
            "trou du cul",
            "trous dcul",
            "trous de cul",
            "trous du cul",
            "vagin",
            "viol",
            "violer",
            "zguègue",
            "zguègues",
        };
    }
}
