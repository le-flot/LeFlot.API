namespace LeFlot.Data
{
    internal class LeFlotProfanityFilter : ProfanityFilter.ProfanityFilter
    {
        public LeFlotProfanityFilter()
            : base()
        {
            AddProfanity(FrenchSwearWords.WordList);
        }
    }
}
