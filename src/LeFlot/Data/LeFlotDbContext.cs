using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace LeFlot.Data
{
    public class LeFlotDbContext : DbContext
    {
        public LeFlotDbContext(DbContextOptions<LeFlotDbContext> options)
            : base(options)
        {
        }

        public DbSet<AppSettingsEntity> AppSettings { get; set; }

        public DbSet<MessageEntity> Messages { get; set; }

        public DbSet<ConnectionRecordEntity> ConnectionRecords { get; set; }

        public DbSet<ThemeEntity> Themes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ThemeEntity>().Property(x => x.ObjectNames)
                .HasConversion(
                    names => JsonSerializer.Serialize(names, null),
                    text => JsonSerializer.Deserialize<List<string>>(text, null),
                    new ValueComparer<List<string>>(
                        (t1, t2) => t1.SequenceEqual(t2),
                        t => t.Aggregate(0, (a, x) => HashCode.Combine(a, x.GetHashCode())),
                        t => t.ToList()));

            var themes = CreateThemes();

            modelBuilder.Entity<ThemeEntity>()
                .HasData(themes);

            modelBuilder.Entity<AppSettingsEntity>()
                .HasData(new AppSettingsEntity { Id = 1, CurrentThemeId = themes.First().Id });
        }

        private static IEnumerable<ThemeEntity> CreateThemes()
        {
            var themes = new List<ThemeEntity>();
            var id = 1;

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "Summer",
                ObjectNames = new List<string>
                {
                    "Benchy",
                    "Boat",
                    "Buoy",
                    "Paper_Boat_1",
                    "Paper_Boat_2",
                    "Paper_Boat_3",
                    "Paper_Boat_4",
                    "RubberDuck_Adult",
                    "RubberDuck_kid",
                    "Tube",
                    "TubeDuck",
                    "tugBoat",
                },
            });

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "Halloween",
                ObjectNames = new List<string>
                {
                    "Ghost_A",
                    "Ghost_B",
                    "Ghost_C",
                    "Ghost_D",
                    "PirateShip",
                    "Pumpkin_A",
                    "Pumpkin_B",
                    "Pumpkin_C",
                    "Pumpkin_D",
                },
            });

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "Fall",
                ObjectNames = new List<string>
                {
                    "Leaf_A",
                    "Leaf_D",
                    "Leaf_G",
                    "Leaf_H",
                    "Leaf_I",
                    "Leaf_O",
                    "Leaf_P",
                },
            });

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "Chinese",
                ObjectNames = new List<string>
                {
                    "Lanterne_Cube_A",
                    "Lanterne_Cube_B",
                    "Lanterne_Cube_C",
                    "Lanterne_Cube_D",
                    "Lanterne_Sphere_A",
                    "Lanterne_Sphere_B",
                    "Lanterne_Sphere_C",
                    "Lanterne_Sphere_D",
                },
            });

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "Christmas",
                ObjectNames = new List<string>
                {
                    "Present_A",
                    "Present_B",
                    "Present_C",
                    "Present_D",
                    "Sled_A",
                    "Sled_B",
                    "Sled_C",
                    "Snowman_A",
                    "Snowman_B",
                    "Snowman_C",
                },
            });

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "Easter",
                ObjectNames = new List<string>
                {
                    "Easter_A",
                    "Easter_B",
                    "Easter_C",
                    "Easter_D",
                    "Rabbit_A_Dark",
                    "Rabbit_A_Milk",
                    "Rabbit_A_White",
                    "Rabbit_B_Dark",
                    "Rabbit_B_Milk",
                    "Rabbit_B_White",
                },
            });

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "StJeanBaptiste",
                ObjectNames = new List<string>
                {
                    "FleurDeLys_A",
                    "FleurDeLys_B",
                    "FleurDeLys_C",
                    "FleurDeLys_D",
                },
            });

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "Canada",
                ObjectNames = new List<string>
                {
                    "MappleLeaf_A",
                    "MappleLeaf_B",
                    "MappleLeaf_C",
                    "MappleLeaf_D",
                },
            });

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "Spring",
                ObjectNames = new List<string>
                {
                    "_Flower_A",
                    "_Flower_B",
                    "_Flower_C",
                    "Boat",
                },
            });

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "Valentine",
                ObjectNames = new List<string>
                {
                    "Heart_A",
                    "Heart_B",
                    "Heart_C",
                    "Heart_D",
                },
            });

            themes.Add(new ThemeEntity
            {
                Id = id++,
                Name = "Winter",
                ObjectNames = new List<string>
                {
                    "Icecube_A",
                    "Icecube_B",
                    "Icecube_C",
                    "RubberDuck_Adult",
                },
            });

            return themes;
        }
    }
}
