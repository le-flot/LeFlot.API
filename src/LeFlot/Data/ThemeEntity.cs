using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace LeFlot.Data
{
    public class ThemeEntity
    {
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        public List<string> ObjectNames { get; set; }
    }
}
