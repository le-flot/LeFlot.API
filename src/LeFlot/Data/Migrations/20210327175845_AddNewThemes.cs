﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LeFlot.Data.Migrations
{
    public partial class AddNewThemes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 1,
                column: "ObjectNames",
                value: "[\"Benchy\",\"Boat\",\"Buoy\",\"Paper boat #1\",\"Paper boat #2\",\"Paper boat #3\",\"Paper boat #4\",\"Adult rubber duck\",\"Kid rubber duck\",\"Tube\",\"Duck tube\",\"Tugboat\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Fall", "[\"Leaf A\",\"Leaf D\",\"Leaf G\",\"Leaf H\",\"Leaf I\",\"Leaf O\",\"Leaf P\"]" });

            migrationBuilder.InsertData(
                table: "Themes",
                columns: new[] { "Id", "Name", "ObjectNames" },
                values: new object[,]
                {
                    { 3, "Halloween", "[\"Pumpkin A\",\"Pumpkin B\",\"Pumpkin C\",\"Pumpkin D\",\"Ghost A\",\"Ghost B\",\"Ghost C\",\"Ghost D\"]" },
                    { 4, "Test", "[\"Test\"]" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 1,
                column: "ObjectNames",
                value: "[\"Benchy\",\"Boat\",\"Buoy\",\"Paper boat #1\",\"Paper boat #2\",\"Paper boat #3\",\"Paper boat #4\",\"Big rubber duck\",\"Small rubber duck\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Test", "[\"Small rubber duck\"]" });
        }
    }
}
