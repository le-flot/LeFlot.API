﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LeFlot.Data.Migrations
{
    public partial class AddNewThemes2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 1,
                column: "ObjectNames",
                value: "[\"Benchy\",\"Boat\",\"Buoy\",\"Paper boat #1\",\"Paper boat #2\",\"Paper boat #3\",\"Paper boat #4\",\"Adult rubber duck\",\"Kid rubber duck\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Halloween", "[\"Ghost A\",\"Ghost B\",\"Ghost C\",\"Ghost D\",\"Pirate ship\",\"Pumpkin A\",\"Pumpkin B\",\"Pumpkin C\",\"Pumpkin D\"]" });

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Fall", "[\"Leaf A\",\"Leaf D\",\"Leaf G\",\"Leaf H\",\"Leaf I\",\"Leaf O\",\"Leaf P\"]" });

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Chinese", "[\"Lanterne cube A\",\"Lanterne cube B\",\"Lanterne cube C\",\"Lanterne cube D\",\"Lanterne sphere A\",\"Lanterne sphere B\",\"Lanterne sphere C\",\"Lanterne sphere D\"]" });

            migrationBuilder.InsertData(
                table: "Themes",
                columns: new[] { "Id", "Name", "ObjectNames" },
                values: new object[,]
                {
                    { 5, "Christmas", "[\"Present A\",\"Present B\",\"Present C\",\"Present D\",\"Sled A\",\"Sled B\",\"Sled C\",\"Snowman A\",\"Snowman B\",\"Snowman C\"]" },
                    { 6, "Easter", "[\"Easter A\",\"Easter B\",\"Easter C\",\"Easter D\",\"Rabbit A dark\",\"Rabbit A milk\",\"Rabbit A white\",\"Rabbit B dark\",\"Rabbit B milk\",\"Rabbit B white\"]" },
                    { 7, "StJeanBaptiste", "[\"Fleur de lys A\",\"Fleur de lys B\",\"Fleur de lys C\",\"Fleur de lys D\",\"Adult rubber duck\",\"Kid rubber duck\"]" },
                    { 8, "Test", "[\"Test\"]" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 1,
                column: "ObjectNames",
                value: "[\"Benchy\",\"Boat\",\"Buoy\",\"Paper boat #1\",\"Paper boat #2\",\"Paper boat #3\",\"Paper boat #4\",\"Adult rubber duck\",\"Kid rubber duck\",\"Tube\",\"Duck tube\",\"Tugboat\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Fall", "[\"Leaf A\",\"Leaf D\",\"Leaf G\",\"Leaf H\",\"Leaf I\",\"Leaf O\",\"Leaf P\"]" });

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Halloween", "[\"Pumpkin A\",\"Pumpkin B\",\"Pumpkin C\",\"Pumpkin D\",\"Ghost A\",\"Ghost B\",\"Ghost C\",\"Ghost D\"]" });

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Test", "[\"Test\"]" });
        }
    }
}
