﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LeFlot.Data.Migrations
{
    public partial class ChangeThemes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.UpdateData(
                table: "AppSettings",
                keyColumn: "Id",
                keyValue: 1,
                column: "CurrentThemeId",
                value: 1);

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Summer", "[\"Benchy\",\"Boat\",\"Buoy\",\"Paper boat #1\",\"Paper boat #2\",\"Paper boat #3\",\"Paper boat #4\",\"Big rubber duck\",\"Small rubber duck\"]" });

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Test", "[\"Small rubber duck\"]" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppSettings",
                keyColumn: "Id",
                keyValue: 1,
                column: "CurrentThemeId",
                value: 3);

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Winter", "[\"boat_1\",\"boat_2\"]" });

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Spring", "[\"boat_1\",\"flower_1\",\"flower_2\"]" });

            migrationBuilder.InsertData(
                table: "Themes",
                columns: new[] { "Id", "Name", "ObjectNames" },
                values: new object[,]
                {
                    { 3, "Summer", "[\"boat_1\",\"duck_1\"]" },
                    { 4, "Fall", "[\"boat_1\",\"leaf_1\"]" },
                    { 5, "Halloween", "[\"boat_1\",\"leaf_1\",\"pumpkin_1\"]" }
                });
        }
    }
}
