﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LeFlot.Data.Migrations
{
    public partial class AddNewThemes3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 1,
                column: "ObjectNames",
                value: "[\"Benchy\",\"Boat\",\"Buoy\",\"Paper_Boat_1\",\"Paper_Boat_2\",\"Paper_Boat_3\",\"Paper_Boat_4\",\"RubberDuck_Adult\",\"RubberDuck_kid\",\"Tube\",\"TubeDuck\",\"tugBoat\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 2,
                column: "ObjectNames",
                value: "[\"Ghost_A\",\"Ghost_B\",\"Ghost_C\",\"Ghost_D\",\"PirateShip\",\"Pumpkin_A\",\"Pumpkin_B\",\"Pumpkin_C\",\"Pumpkin_D\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 3,
                column: "ObjectNames",
                value: "[\"Leaf_A\",\"Leaf_D\",\"Leaf_G\",\"Leaf_H\",\"Leaf_I\",\"Leaf_O\",\"Leaf_P\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 4,
                column: "ObjectNames",
                value: "[\"Lanterne_Cube_A\",\"Lanterne_Cube_B\",\"Lanterne_Cube_C\",\"Lanterne_Cube_D\",\"Lanterne_Sphere_A\",\"Lanterne_Sphere_B\",\"Lanterne_Sphere_C\",\"Lanterne_Sphere_D\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 5,
                column: "ObjectNames",
                value: "[\"Present_A\",\"Present_B\",\"Present_C\",\"Present_D\",\"Sled_A\",\"Sled_B\",\"Sled_C\",\"Snowman_A\",\"Snowman_B\",\"Snowman_C\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 6,
                column: "ObjectNames",
                value: "[\"Easter_A\",\"Easter_B\",\"Easter_C\",\"Easter_D\",\"Rabbit_A_Dark\",\"Rabbit_A_Milk\",\"Rabbit_A_White\",\"Rabbit_B_Dark\",\"Rabbit_B_Milk\",\"Rabbit_B_White\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 7,
                column: "ObjectNames",
                value: "[\"FleurDeLys_A\",\"FleurDeLys_B\",\"FleurDeLys_C\",\"FleurDeLys_D\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Canada", "[\"MappleLeaf_A\",\"MappleLeaf_B\",\"MappleLeaf_C\",\"MappleLeaf_D\"]" });

            migrationBuilder.InsertData(
                table: "Themes",
                columns: new[] { "Id", "Name", "ObjectNames" },
                values: new object[,]
                {
                    { 9, "Spring", "[\"_Flower_A\",\"_Flower_B\",\"_Flower_C\",\"Boat\"]" },
                    { 10, "Valentine", "[\"Heart_A\",\"Heart_B\",\"Heart_C\",\"Heart_D\"]" },
                    { 11, "Winter", "[\"Icecube_A\",\"Icecube_B\",\"Icecube_C\",\"RubberDuck_Adult\"]" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 1,
                column: "ObjectNames",
                value: "[\"Benchy\",\"Boat\",\"Buoy\",\"Paper_Boat_1\",\"Paper_Boat_2\",\"Paper boat #3\",\"Paper boat #4\",\"Adult rubber duck\",\"Kid rubber duck\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 2,
                column: "ObjectNames",
                value: "[\"Ghost A\",\"Ghost B\",\"Ghost C\",\"Ghost D\",\"Pirate ship\",\"Pumpkin A\",\"Pumpkin B\",\"Pumpkin C\",\"Pumpkin D\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 3,
                column: "ObjectNames",
                value: "[\"Leaf A\",\"Leaf D\",\"Leaf G\",\"Leaf H\",\"Leaf I\",\"Leaf O\",\"Leaf P\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 4,
                column: "ObjectNames",
                value: "[\"Lanterne cube A\",\"Lanterne cube B\",\"Lanterne cube C\",\"Lanterne cube D\",\"Lanterne sphere A\",\"Lanterne sphere B\",\"Lanterne sphere C\",\"Lanterne sphere D\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 5,
                column: "ObjectNames",
                value: "[\"Present A\",\"Present B\",\"Present C\",\"Present D\",\"Sled A\",\"Sled B\",\"Sled C\",\"Snowman A\",\"Snowman B\",\"Snowman C\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 6,
                column: "ObjectNames",
                value: "[\"Easter A\",\"Easter B\",\"Easter C\",\"Easter D\",\"Rabbit A dark\",\"Rabbit A milk\",\"Rabbit A white\",\"Rabbit B dark\",\"Rabbit B milk\",\"Rabbit B white\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 7,
                column: "ObjectNames",
                value: "[\"Fleur de lys A\",\"Fleur de lys B\",\"Fleur de lys C\",\"Fleur de lys D\",\"Adult rubber duck\",\"Kid rubber duck\"]");

            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "Name", "ObjectNames" },
                values: new object[] { "Test", "[\"Test\"]" });
        }
    }
}
