﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LeFlot.Data.Migrations
{
    public partial class updateJR : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 1,
                column: "ObjectNames",
                value: "[\"Benchy\",\"Boat\",\"Buoy\",\"Paper_Boat_1\",\"Paper_Boat_2\",\"Paper boat #3\",\"Paper boat #4\",\"Adult rubber duck\",\"Kid rubber duck\"]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Themes",
                keyColumn: "Id",
                keyValue: 1,
                column: "ObjectNames",
                value: "[\"Benchy\",\"Boat\",\"Buoy\",\"Paper boat #1\",\"Paper boat #2\",\"Paper boat #3\",\"Paper boat #4\",\"Adult rubber duck\",\"Kid rubber duck\"]");
        }
    }
}
