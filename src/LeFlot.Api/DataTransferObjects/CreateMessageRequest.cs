using System.ComponentModel.DataAnnotations;

namespace LeFlot.Api.DataTransferObjects
{
    public class CreateMessageRequest
    {
        /// <summary>
        /// The message text.
        /// </summary>
        [Required]
        [MinLength(2)]
        [MaxLength(100)]
        public string Text { get; set; }

        /// <summary>
        /// The message object ID.
        /// </summary>
        public int ObjectId { get; set; }
    }
}
