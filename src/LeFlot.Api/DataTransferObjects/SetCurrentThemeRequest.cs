namespace LeFlot.Api.DataTransferObjects
{
    public class SetCurrentThemeRequest
    {
        /// <summary>
        /// The current theme ID.
        /// </summary>
        public int ThemeId { get; set; }
    }
}
