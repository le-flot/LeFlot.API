using AutoMapper;
using LeFlot.Data;
using LeFlot.Api.Models;
using LeFlot.Models;

namespace LeFlot.Api.Mappers
{
    /// <summary>
    /// Contains mapping profiles for the whole assembly.
    /// We may split it into multiple profiles later on.
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MappingProfile"/> class.
        /// </summary>
        public MappingProfile()
        {
            CreateMap<FilteredMessage, Message>();
            CreateMap<ThemeEntity, Theme>();
        }
    }
}
