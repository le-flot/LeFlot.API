using System.Collections.Generic;

namespace LeFlot.Api.Models
{
    public class Theme
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public IEnumerable<string> ObjectNames { get; set; }
    }
}
