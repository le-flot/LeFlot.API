using System;

namespace LeFlot.Api.Models
{
    public class Message
    {
        public int Id { get; set; }

        public string OriginText { get; set; }

        public string Text { get; set; }

        public int ObjectId { get; set; }

        public DateTimeOffset CreatedAt { get; set; }
    }
}
