using System.Collections.Generic;

namespace LeFlot.Api.Models
{
    public class DataList<T>
    {
        public DataList(IEnumerable<T> data)
        {
            Data = data;
        }

        public IEnumerable<T> Data { get; set; }
    }
}
