using System;
using System.Net;
using System.Threading.Tasks;
using LeFlot.Commands;
using LeFlot.Data;
using LeFlot.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace LeFlot.Api.Filters
{
    public class CooldownAttribute : ActionFilterAttribute
    {
        public float _duration;

        public CooldownAttribute(float duration)
        {
            _duration = duration;
        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            var sender = context.HttpContext.RequestServices.GetService<ISender>();

            await RemoveDayOldConnectionRecordsAsync(sender);

            var connectionRecord =
                await GetConnectionRecordAsync(sender, context.HttpContext.Connection.RemoteIpAddress);

            bool canExecuteAction;
            if (connectionRecord == null)
            {
                await CreateConnectionRecordAsync(sender, context.HttpContext.Connection.RemoteIpAddress);
                canExecuteAction = true;
            }
            else
            {
                canExecuteAction = await CheckAndUpdateConnectionRecordAsync(sender, connectionRecord);
            }

            if (!canExecuteAction)
            {
                context.Result = new StatusCodeResult(StatusCodes.Status403Forbidden);
            }
            else
            {
                await next();
            }
        }

        private static async Task RemoveDayOldConnectionRecordsAsync(ISender sender)
        {
            var command = new RemoveOldConnectionRecordsCommand(DateTimeOffset.Now - TimeSpan.FromDays(1));
            await sender.Send(command);
        }

        private static async Task<ConnectionRecordEntity> GetConnectionRecordAsync(
            ISender sender,
            IPAddress ipAddress)
        {
            var query = new GetConnectionRecordQuery(ipAddress);
            return await sender.Send(query);
        }

        private static async Task<ConnectionRecordEntity> CreateConnectionRecordAsync(
            ISender sender,
            IPAddress ipAddress)
        {
            var command = new CreateConnectionRecordCommand(ipAddress);
            return await sender.Send(command);
        }

        private async Task<bool> CheckAndUpdateConnectionRecordAsync(
            ISender sender,
            ConnectionRecordEntity connectionRecord)
        {
            var now = DateTimeOffset.Now;
            var cooldownTime = now.Add(-TimeSpan.FromSeconds(_duration));
            var updated = connectionRecord.LastAccess < cooldownTime;
            if (updated)
            {
                var command = new UpdateConnectionRecordLastAccessCommand(connectionRecord.Id);
                await sender.Send(command);
            }

            return updated;
        }
    }
}
