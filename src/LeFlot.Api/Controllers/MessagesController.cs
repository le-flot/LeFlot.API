﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LeFlot.Api.DataTransferObjects;
using LeFlot.Api.Filters;
using LeFlot.Api.Models;
using LeFlot.Commands;
using LeFlot.Exceptions;
using LeFlot.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LeFlot.Api.Controllers
{
    /// <summary>
    /// Operations on the messages.
    /// </summary>
    [Route("api/messages")]
    [ApiController]
    public class MessagesController : ControllerBase
    {
        private readonly ISender _sender;
        private readonly IMapper _mapper;

        public MessagesController(ISender sender, IMapper mapper)
        {
            _sender = sender;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the messages (ordered by descending creation date).
        /// </summary>
        /// <param name="cancellationToken">The cancellation token.</param>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<DataList<Message>>> GetAllAsync(CancellationToken cancellationToken)
        {
            // Remove messages that are a week old.
            var command = new RemoveOldMessagesCommand(DateTimeOffset.Now - TimeSpan.FromDays(7));
            await _sender.Send(command, cancellationToken);

            // Get messages.
            var query = new GetMessagesQuery();
            var response = await _sender.Send(query, cancellationToken);

            var messages = _mapper.Map<IReadOnlyList<Message>>(response);
            return Ok(new DataList<Message>(messages));
        }

        /// <summary>
        /// Gets a message by its ID.
        /// </summary>
        /// <param name="id">The message ID.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Message>> GetByIdAsync(
            [FromRoute] int id,
            CancellationToken cancellationToken)
        {
            var query = new GetMessageQuery(id);

            try
            {
                var response = await _sender.Send(query, cancellationToken);
                return Ok(_mapper.Map<Message>(response));
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Adds a new message.
        /// </summary>
        /// <param name="request">The request body.</param>
        /// <param name="cancellationToken">The cancellation token.</param>
        [HttpPost]
        [Cooldown(30.0f)]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<ActionResult<Message>> CreateAsync(
            [FromBody] CreateMessageRequest request,
            CancellationToken cancellationToken)
        {
            var command = new CreateMessageCommand(request.Text)
            {
                ObjectId = request.ObjectId,
            };

            var response = await _sender.Send(command, cancellationToken);

            return CreatedAtAction(
                nameof(GetByIdAsync),
                new { id = response.Id },
                _mapper.Map<Message>(response));
        }
    }
}
