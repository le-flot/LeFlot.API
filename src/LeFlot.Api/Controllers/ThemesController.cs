using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using LeFlot.Api.DataTransferObjects;
using LeFlot.Api.Models;
using LeFlot.Commands;
using LeFlot.Exceptions;
using LeFlot.Queries;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LeFlot.Api.Controllers
{
    /// <summary>
    /// Operations on the themes.
    /// </summary>
    [Route("api/themes")]
    [ApiController]
    public class ThemesController : ControllerBase
    {
        private readonly ISender _sender;
        private readonly IMapper _mapper;

        public ThemesController(ISender sender, IMapper mapper)
        {
            _sender = sender;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets the themes.
        /// </summary>
        [HttpGet]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<DataList<Theme>>> GetAllAsync(CancellationToken cancellationToken)
        {
            var query = new GetThemesQuery();
            var response = await _sender.Send(query, cancellationToken);

            var themes = _mapper.Map<IReadOnlyList<Theme>>(response);
            return Ok(new DataList<Theme>(themes));
        }

        /// <summary>
        /// Gets a theme.
        /// </summary>
        [HttpGet("{id}")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Theme>> GetById([FromRoute] int id, CancellationToken cancellationToken)
        {
            var query = new GetThemeQuery(id);

            try
            {
                var response = await _sender.Send(query, cancellationToken);
                return Ok(_mapper.Map<Theme>(response));
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Gets the current theme.
        /// </summary>
        [HttpGet("current")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Theme>> GetCurrentAsync(CancellationToken cancellationToken)
        {
            var query = new GetCurrentThemeQuery();
            var response = await _sender.Send(query, cancellationToken);

            return Ok(_mapper.Map<Theme>(response));
        }


        /// <summary>
        /// Gets the current theme.
        /// </summary>
        [HttpPut("current")]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> SetCurrentAsync(
            [FromBody] SetCurrentThemeRequest request,
            CancellationToken cancellationToken)
        {
            var query = new UpdateCurrentThemeCommand(request.ThemeId);

            try
            {
                await _sender.Send(query, cancellationToken);
            }
            catch (NotFoundException)
            {
                return NotFound();
            }

            return NoContent();
        }
    }
}
