# Le Flot - API

Le Flot est un projet artistique, multimédia et interactif sponsorisé par la
ville de Sherbrooke.

Ce dépôt contient le code source du backend du projet. Le backend est l'élément
permettant de communiquer avec le module Unity situé ici:
https://gitlab.com/le-flot/le-flot

# Développement

## Prérequis

Voici la liste des logiciels nécessaires pour contribuer au projet:

* [Git](https://git-scm.com/downloads)
* [.NET 5.0](https://dotnet.microsoft.com/download/dotnet-core)
* [Docker](https://www.docker.com/get-started)

## Mise en place

Cloner le dépôt Git (depuis SourceTree si vous utilisez cette interface).

## Démarrer le backend

Le backend nécessite une base de données (DB). Docker est utilisé pour lancer
localement la DB.

Sachant qu'un développeur aura souvent à redémarrer une nouvelle DB, un script
est mis à disposition pour simplifier la procédure:

    ./restart-db.sh

Ensuite, pour démarrer le backend, lancer le projet dans un IDE ou entrer cette
ligne de commande dans un terminal:

    dotnet run -p src/LeFlot.Api

Une interface web pour tester l'API est alors disponible ici:
http://localhost:5000/swagger.

## Gestion de la base de données

LeFlot utilise l'outil `dotnet-ef` pour gérer les mise-à-jours de la BD.

### Installation de `dotnet-ef`

    dotnet tool install --global dotnet-ef

### Utiliser `dotnet-ef`

Pour appliquer les migrations, il faut que la BD soit lancée (avec
`./restart-db.sh`).

### Lister les migrations

Pour lister toutes les migrations disponibles, lancer:

    dotnet ef migrations list -p src/LeFlot -s src/LeFlot.Api

### Mettre à jour la BD

Pour mettre à jour la BD, lancer:

    dotnet ef database update -p src/LeFlot -s src/LeFlot.Api [NomDeLaMigration]

L'argument `NomDeLaMigration` est optionnel, s'il n'est pas défini, la BD sera
mise à jour avec la dernière migration.

Les mise-à-jour étant bi-directionnelles, il est possible de donner une
migration plus ancienne ou plus récente.

### Ajouter une nouvelle migration

Après avoir fait toutes les modifications dans le code, lancer:

    dotnet ef migrations add -p src/LeFlot -s src/LeFlot.Api -o Data/Migrations [NomDeLaMigration]

Les nouveaux fichiers seront ajoutés dans `src/LeFlot/Data/Migrations`.

### Supprimer une migration

Pour supprimer la plus récente migration, lancer:

    dotnet ef migrations remove -p src/LeFlot -s src/LeFlot.Api

# Déploiement

### Prérequis

Pour simplifier le déploiement de l'API et ses dépendances, nous avons choisi l'outil
[Docker Compose](https://docs.docker.com/compose/). Utilisez
[cette page de documentation](https://docs.docker.com/compose/install/) pour l'installer
dépendamment de votre OS.

### Lancer le déploiement

La première fois, il faut se loger avec 

    docker login registry.gitlab.com -u @julienrobert -p vyptqKfR5F_1QdRKZfVU

La définition du déploiement se trouve dans le fichier
[`docker-compose.yml`](./docker-compose.yml). Pour déployer il suffit de lancer cette commande:

    docker-compose up -d

Après le premier lancement, il est nécessaire d'effectuer la migration de la base de donnée en roulant le script suivant:

```
sh migrate.sh
```

Sur Windows exécuter migrate.bat