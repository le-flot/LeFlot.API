using System;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Queries;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Queries
{
    public class GetThemesQueryTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly GetThemesQuery.Handler _handler;

        public GetThemesQueryTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new GetThemesQuery.Handler(_dbContextProvider.Context);
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenDefaultThemesExist_ThenReturnsDefaultThemes()
        {
            // Arrange
            var query = new GetThemesQuery();

            // Act
            var result = await _handler.Handle(query, default);

            // Assert
            result.Should().NotBeNullOrEmpty();
        }
    }
}
