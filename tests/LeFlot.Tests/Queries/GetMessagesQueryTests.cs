using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Data;
using LeFlot.Models;
using LeFlot.Queries;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Queries
{
    public class GetMessagesQueryTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly GetMessagesQuery.Handler _handler;

        public GetMessagesQueryTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new GetMessagesQuery.Handler(_dbContextProvider.Context, new LeFlotProfanityFilter());
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenMessagesExist_ThenReturnsMessagesOrderedByDateDesc()
        {
            // Arrange
            var now = DateTimeOffset.Now;
            var messages = new[]
            {
                new MessageEntity
                {
                    Text = "Message text #1",
                    ObjectId = 0,
                    CreatedAt = now.AddDays(-1),
                },
                new MessageEntity
                {
                    Text = "Message text #2",
                    ObjectId = 1,
                    CreatedAt = now,
                },
            };

            _dbContextProvider.Arrange(context =>
            {
                context.Messages.AddRange(messages);
            });

            var query = new GetMessagesQuery();

            // Act
            var result = await _handler.Handle(query, default);

            // Assert
            result.Should().HaveCount(messages.Length);
            result.ElementAt(0).Should().BeEquivalentTo(messages[1]);
            result.ElementAt(1).Should().BeEquivalentTo(messages[0]);
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenMessageWithProfanity_ThenReturnsCensoredText()
        {
            // Arrange
            var message = new MessageEntity
            {
                Text = "Bite text",
                ObjectId = 0,
            };

            var expectedCensoredText = "**** text";

            _dbContextProvider.Arrange(context =>
            {
                context.Messages.Add(message);
            });

            var query = new GetMessagesQuery();

            // Act
            var result = await _handler.Handle(query, default);

            // Assert
            result.Should().HaveCount(1);
            result.ElementAt(0).Should().Match<FilteredMessage>(x =>
                x.OriginText == message.Text &&
                x.Text == expectedCensoredText);
        }
    }
}
