using System;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Data;
using LeFlot.Exceptions;
using LeFlot.Models;
using LeFlot.Queries;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Queries
{
    public class GetMessageQueryTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly GetMessageQuery.Handler _handler;

        public GetMessageQueryTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new GetMessageQuery.Handler(_dbContextProvider.Context, new LeFlotProfanityFilter());
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public void WhenHandlingQuery_GivenMessageDoesNotExist_ThenThrowsNotFoundException()
        {
            // Arrange
            var query = new GetMessageQuery(100);

            // Act / Assert
            Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(query, default));
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenMessageExists_ThenReturnsMessage()
        {
            // Arrange
            var message = new MessageEntity
            {
                Text = "Message text",
                ObjectId = 0,
            };

            _dbContextProvider.Arrange(context =>
            {
                context.Messages.Add(message);
            });

            var query = new GetMessageQuery(message.Id);

            // Act
            var result = await _handler.Handle(query, default);

            // Assert
            result.Should()
                .NotBeNull().And
                .Match<FilteredMessage>(x =>
                    x.Id == message.Id &&
                    x.Text == message.Text &&
                    x.ObjectId == message.ObjectId);
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenMessageWithProfanity_ThenReturnsCensoredText()
        {
            // Arrange
            var message = new MessageEntity
            {
                Text = "Bite text",
                ObjectId = 0,
            };

            var expectedCensoredText = "**** text";

            _dbContextProvider.Arrange(context =>
            {
                context.Messages.Add(message);
            });

            var query = new GetMessageQuery(message.Id);

            // Act
            var result = await _handler.Handle(query, default);

            // Assert
            result.Should()
                .NotBeNull().And
                .Match<FilteredMessage>(x =>
                    x.OriginText == message.Text &&
                    x.Text == expectedCensoredText);
        }
    }
}
