using System;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Data;
using LeFlot.Queries;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Queries
{
    public class GetConnectionRecordQueryTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly GetConnectionRecordQuery.Handler _handler;

        public GetConnectionRecordQueryTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new GetConnectionRecordQuery.Handler(_dbContextProvider.Context);
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenIpAddressDoesNotExist_ThenReturnsNull()
        {
            // Arrange
            var connectionRecord = new ConnectionRecordEntity
            {
                IpAddress = new IPAddress(new byte[] { 127, 0, 0, 1 }),
            };

            var query = new GetConnectionRecordQuery(connectionRecord.IpAddress);

            // Act
            var result = await _handler.Handle(query, default);

            // Assert
            result.Should().BeNull();
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenIpAddressExists_ThenReturnsConnectionRecord()
        {
            // Arrange
            var connectionRecord = new ConnectionRecordEntity
            {
                IpAddress = new IPAddress(new byte[] { 127, 0, 0, 1 }),
                LastAccess = DateTimeOffset.Now,
            };

            _dbContextProvider.Arrange(context =>
            {
                context.ConnectionRecords.Add(connectionRecord);
            });

            var query = new GetConnectionRecordQuery(connectionRecord.IpAddress);

            // Act
            var result = await _handler.Handle(query, default);

            // Assert
            result.Should().BeEquivalentTo(connectionRecord);
        }
    }
}
