using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Data;
using LeFlot.Exceptions;
using LeFlot.Queries;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Queries
{
    public class GetThemeQueryTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly GetThemeQuery.Handler _handler;

        public GetThemeQueryTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new GetThemeQuery.Handler(_dbContextProvider.Context);
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public void WhenHandlingQuery_GivenThemeDoesNotExist_ThenThrowsNotFoundException()
        {
            // Arrange
            var query = new GetThemeQuery(100);

            // Act / Assert
            Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(query, default));
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenThemeExists_ThenReturnsTheme()
        {
            // Arrange
            var theme = new ThemeEntity
            {
                Name = "TestTheme",
                ObjectNames = new List<string> { "model_1", "model_2", "model_3" },
            };

            _dbContextProvider.Arrange(context =>
            {
                context.Themes.Add(theme);
            });

            var query = new GetThemeQuery(theme.Id);

            // Act
            var result = await _handler.Handle(query, default);

            // Assert
            result.Should()
                .NotBeNull().And
                .Match<ThemeEntity>(x =>
                    x.Id == theme.Id &&
                    x.Name == theme.Name &&
                    x.ObjectNames.Count == theme.ObjectNames.Count);

            if (theme.ObjectNames.Any())
            {
                result.ObjectNames.Should().Contain(theme.ObjectNames);
            }
            else
            {
                result.ObjectNames.Should().BeEmpty();
            }
        }
    }
}
