using System;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Queries;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Queries
{
    public class GetCurrentThemeQueryTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly GetCurrentThemeQuery.Handler _handler;

        public GetCurrentThemeQueryTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new GetCurrentThemeQuery.Handler(_dbContextProvider.Context);
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenDefaultAppSettings_ThenReturnsCurrentTheme()
        {
            // Arrange
            var query = new GetCurrentThemeQuery();

            // Act
            var result = await _handler.Handle(query, default);

            // Assert
            result.Should().NotBeNull();
        }
    }
}
