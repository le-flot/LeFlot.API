using System;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Commands;
using LeFlot.Data;
using LeFlot.Models;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Commands
{
    public class CreateMessageCommandTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly CreateMessageCommand.Handler _handler;

        public CreateMessageCommandTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new CreateMessageCommand.Handler(_dbContextProvider.Context, new LeFlotProfanityFilter());
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenValidText_ThenCreatesMessage()
        {
            // Arrange
            var text = "New message";
            var objectId = 2;
            var now = DateTimeOffset.Now;

            var command = new CreateMessageCommand(text)
            {
                ObjectId = objectId,
            };

            // Act
            var result = await _handler.Handle(command, default);

            // Assert
            result.Should().NotBeNull().And
                .Match<FilteredMessage>(x =>
                    x.OriginText == text &&
                    x.Text == text &&
                    x.ObjectId == objectId &&
                    x.CreatedAt >= now);
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenTextWithProfanity_ThenCreatesMessageWithCensoredText()
        {
            // Arrange
            var text = "Bite text";
            var expectedCensoredText = "**** text";

            var command = new CreateMessageCommand(text);

            // Act
            var result = await _handler.Handle(command, default);

            // Assert
            result.Should().NotBeNull().And
                .Match<FilteredMessage>(x =>
                    x.OriginText == text &&
                    x.Text == expectedCensoredText);
        }
    }
}
