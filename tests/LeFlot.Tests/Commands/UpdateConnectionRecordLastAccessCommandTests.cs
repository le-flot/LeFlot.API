using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Commands;
using LeFlot.Data;
using LeFlot.Tests.Data;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace LeFlot.Tests.Commands
{
    public class UpdateConnectionRecordLastAccessCommandTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly UpdateConnectionRecordLastAccessCommand.Handler _handler;

        public UpdateConnectionRecordLastAccessCommandTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new UpdateConnectionRecordLastAccessCommand.Handler(_dbContextProvider.Context);
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenDate_ThenRemovesConnectionsBeforeDate()
        {
            // Arrange
            var now = DateTimeOffset.Now;
            var connectionRecords = new[]
            {
                new ConnectionRecordEntity
                {
                    IpAddress = new IPAddress(new byte[] { 192, 168, 0, 1 }),
                    LastAccess = now.AddDays(-2),
                },
                new ConnectionRecordEntity
                {
                    IpAddress = new IPAddress(new byte[] { 192, 168, 0, 2 }),
                    LastAccess = now.AddDays(-1),
                },
            };

            _dbContextProvider.Arrange(context =>
            {
                context.ConnectionRecords.AddRange(connectionRecords);
            });

            var command = new UpdateConnectionRecordLastAccessCommand(connectionRecords[0].Id);

            // Act
            var result = await _handler.Handle(command, default);

            // Assert
            await _dbContextProvider.AssertAsync(async context =>
            {
                var updatedConnectionRecord = await context.ConnectionRecords
                    .Where(x => x.Id == connectionRecords[0].Id)
                    .FirstOrDefaultAsync();

                updatedConnectionRecord.Should().NotBeNull().And
                    .Match<ConnectionRecordEntity>(x =>
                        x.LastAccess >= now);
            });
        }
    }
}
