using System;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Commands;
using LeFlot.Data;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Commands
{
    public class RemoveOldConnectionRecordsCommandTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly RemoveOldConnectionRecordsCommand.Handler _handler;

        public RemoveOldConnectionRecordsCommandTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new RemoveOldConnectionRecordsCommand.Handler(_dbContextProvider.Context);
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenDate_ThenRemovesConnectionsBeforeDate()
        {
            // Arrange
            var removeBefore = DateTimeOffset.Now.AddDays(-1);
            var connectionRecords = new[]
            {
                new ConnectionRecordEntity
                {
                    IpAddress = new IPAddress(new byte[] { 192, 168, 0, 1 }),
                    LastAccess = removeBefore.AddDays(-1),
                },
                new ConnectionRecordEntity
                {
                    IpAddress = new IPAddress(new byte[] { 192, 168, 0, 2 }),
                    LastAccess = removeBefore,
                },
                new ConnectionRecordEntity
                {
                    IpAddress = new IPAddress(new byte[] { 192, 168, 0, 2 }),
                    LastAccess = removeBefore.AddDays(1),
                },
            };

            _dbContextProvider.Arrange(context =>
            {
                context.ConnectionRecords.AddRange(connectionRecords);
            });

            var command = new RemoveOldConnectionRecordsCommand(removeBefore);

            // Act
            var result = await _handler.Handle(command, default);

            // Assert
            await _dbContextProvider.AssertAsync(context =>
            {
                context.ConnectionRecords.Should().HaveCount(2);
                return Task.CompletedTask;
            });
        }
    }
}
