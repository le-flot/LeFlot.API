using System;
using System.Net;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Commands;
using LeFlot.Data;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Commands
{
    public class CreateConnectionRecordCommandTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly CreateConnectionRecordCommand.Handler _handler;

        public CreateConnectionRecordCommandTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new CreateConnectionRecordCommand.Handler(_dbContextProvider.Context);
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenIpAddress_ThenCreatesConnectionRecord()
        {
            // Arrange
            var ipAddress = new IPAddress(new byte[] { 127, 0, 0, 1 });
            var now = DateTimeOffset.Now;

            var command = new CreateConnectionRecordCommand(ipAddress);

            // Act
            var result = await _handler.Handle(command, default);

            // Assert
            result.Should().NotBeNull().And
                .Match<ConnectionRecordEntity>(x =>
                    x.IpAddress == ipAddress &&
                    x.LastAccess >= now);
        }
    }
}
