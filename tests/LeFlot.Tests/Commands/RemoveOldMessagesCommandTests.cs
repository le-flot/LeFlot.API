using System;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Commands;
using LeFlot.Data;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Commands
{
    public class RemoveOldMessagesCommandTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly RemoveOldMessagesCommand.Handler _handler;

        public RemoveOldMessagesCommandTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new RemoveOldMessagesCommand.Handler(_dbContextProvider.Context);
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public async Task WhenHandlingQuery_GivenDate_ThenRemovesMessagesBeforeDate()
        {
            // Arrange
            var removeBefore = DateTimeOffset.Now.AddDays(-1);
            var messages = new[]
            {
                new MessageEntity
                {
                    Text = "Message text #1",
                    CreatedAt = removeBefore.AddDays(-1),
                },
                new MessageEntity
                {
                    Text = "Message text #2",
                    CreatedAt = removeBefore,
                },
                new MessageEntity
                {
                    Text = "Message text #3",
                    CreatedAt = removeBefore.AddDays(1),
                },
            };

            _dbContextProvider.Arrange(context =>
            {
                context.Messages.AddRange(messages);
            });

            var command = new RemoveOldMessagesCommand(removeBefore);

            // Act
            var result = await _handler.Handle(command, default);

            // Assert
            await _dbContextProvider.AssertAsync(context =>
            {
                context.Messages.Should().HaveCount(2);
                return Task.CompletedTask;
            });
        }
    }
}
