using System;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using LeFlot.Commands;
using LeFlot.Exceptions;
using LeFlot.Tests.Data;
using Xunit;

namespace LeFlot.Tests.Commands
{
    public class UpdateCurrentThemeCommandTests : IDisposable
    {
        private readonly SqliteProjectsDbContextProvider _dbContextProvider;
        private readonly UpdateCurrentThemeCommand.Handler _handler;

        public UpdateCurrentThemeCommandTests()
        {
            _dbContextProvider = new SqliteProjectsDbContextProvider();
            _handler = new UpdateCurrentThemeCommand.Handler(_dbContextProvider.Context);
        }

        public void Dispose()
        {
            _dbContextProvider.Dispose();
        }

        [Fact]
        public void WhenHandlingCommand_GivenInvalidThemeId_ThenThrowsException()
        {
            // Arrange
            int invalidThemeId = -1;
            _dbContextProvider.Arrange(context =>
            {
                var themes = context.Themes.ToList();
                invalidThemeId = themes.Count;
            });

            var command = new UpdateCurrentThemeCommand(invalidThemeId);

            // Act / Assert
            Assert.ThrowsAsync<NotFoundException>(() => _handler.Handle(command, default));
        }

        [Fact]
        public async Task WhenHandlingCommand_GivenValidThemeId_ThenReturnsSuccess()
        {
            // Arrange
            int newThemeId = -1;
            _dbContextProvider.Arrange(context =>
            {
                var themes = context.Themes.ToList();
                var appSettings = context.AppSettings.First();
                newThemeId = (appSettings.CurrentThemeId + 1) % themes.Count;
            });

            var command = new UpdateCurrentThemeCommand(newThemeId);

            // Act
            var result = await _handler.Handle(command, default);

            // Assert
            await _dbContextProvider.AssertAsync(context =>
            {
                var appSettings = context.AppSettings.First();
                var currentThemeId = appSettings.CurrentThemeId;

                currentThemeId.Should().Be(newThemeId);

                return Task.CompletedTask;
            });
        }
    }
}
