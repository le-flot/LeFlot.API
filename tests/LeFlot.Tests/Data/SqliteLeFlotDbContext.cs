using System.Data.Common;
using LeFlot.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Flo.Tests.Data
{
    internal sealed class SqliteLeFlotDbContext : LeFlotDbContext
    {
        private readonly DbConnection _connection;

        public SqliteLeFlotDbContext(DbConnection connection, DbContextOptions<LeFlotDbContext> options)
            : base(options)
        {
            _connection = connection;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlite(_connection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ConnectionRecordEntity>(entityBuilder =>
            {
                entityBuilder.Property(x => x.LastAccess)
                    .HasConversion(new DateTimeOffsetToStringConverter());
            });

            modelBuilder.Entity<MessageEntity>(entityBuilder =>
            {
                entityBuilder.Property(x => x.CreatedAt)
                    .HasConversion(new DateTimeOffsetToStringConverter());
            });
        }
    }
}
