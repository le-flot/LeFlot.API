using System;
using System.Threading.Tasks;
using Flo.Tests.Data;
using LeFlot.Data;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace LeFlot.Tests.Data
{
    internal sealed class SqliteProjectsDbContextProvider : IDisposable
    {
        private const string ConnectionString = "Filename=:memory:";
        private readonly SqliteConnection _connection;
        private readonly DbContextOptions<LeFlotDbContext> _options;

        public SqliteProjectsDbContextProvider()
        {
            _connection = new SqliteConnection(ConnectionString);
            _connection.Open();

            _options = new DbContextOptionsBuilder<LeFlotDbContext>()
                .UseSqlite(_connection)
                .Options;

            Context = CreateContext();
            Context.Database.EnsureCreated();
        }

        public LeFlotDbContext Context { get; }

        public void Arrange(Action<LeFlotDbContext> arrange)
        {
            using (var context = CreateContext())
            {
                arrange(context);

                context.SaveChanges();
            }
        }

        public async Task AssertAsync(Func<LeFlotDbContext, Task> assert)
        {
            using (var context = CreateContext())
            {
                await assert(context);
            }
        }

        public LeFlotDbContext CreateContext()
        {
            return new SqliteLeFlotDbContext(_connection, _options);
        }

        public void Dispose()
        {
            Context.Dispose();

            _connection.Close();
            _connection.Dispose();
        }
    }
}
